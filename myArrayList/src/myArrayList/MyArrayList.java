import java.util.Arrays;

public class MyArrayList
{
	public int size = 50;
	private int[] values = new int[size];
	public static int i = 0;

	public void resizeArray()
	{
		int[] dummy = new int[size];
		System.arraycopy(values, 0, dummy, 0, values.length);
		size = size + 25;
		values = new int[size];
		Arrays.fill(values, 99999);
		System.arraycopy(dummy, 0, values, 0, dummy.length);

	}

	public void assignArray()
	{
		Arrays.fill(values, 99999);
	}

	public void insertSorted(int newValue)
	{
		int c = 0;
		for (i = 0; i < size; i++)
		{
			if (values[i] == 99999)
			{
				c = i;
				break;
			} else
				c = size;

		}
		if (c >= size)
		{
			resizeArray();
			values[i] = newValue;
			Arrays.sort(values);
			i++;
			size = arraySize();
		} else
		{
			values[i] = newValue;
			Arrays.sort(values);
			i++;
		}
	}

	public void removeValue(int value)
	{
		for (int i = 0; i < size; i++)
		{
			if (value == values[i])
			{
				values[i] = 99999;
			}
		}
		Arrays.sort(values);
		i--;
	}

	public int indexOf(int value)
	{
		for (int i = 0; i < size; i++)
		{
			if (values[i] == value)
			{
				return i;
			}

		}
		return -1;
	}

	public int arraySize()
	{
		return size;
	}

	public int size()
	{
		Arrays.sort(values);
		int c = 0;
		for (i = 0; i < size; i++)
		{
			if (values[i] == 99999)
			{
				c = i;
				break;
			} else
				c = size;

		}
		return c;
	}

	public int sum()
	{
		Arrays.sort(values);
		int c = 0;
		int sum = 0;
		for (i = 0; i < size; i++)
		{
			if (values[i] == 99999)
			{
				c = i;
				break;
			} else
				c = size;
		}

		for (i = 0; i < c; i++)
		{
			sum += values[i];
		}
		return sum;
	}

	@Override
	public String toString()
	{
		System.out.print("[");
		for (i = 0; i < size; i++)
		{
			if (values[i] == 99999)
			{
				if (i == size - 1)
				{
					System.out.print("0");
				} else
					System.out.print("0, ");
			} else
			{
				if (i == size - 1)
				{
					System.out.print(values[i]);
				} else
					System.out.print(values[i] + ", ");
			}
		}
		System.out.print("]");
		return null;
	}
}
