import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class Results implements FileDisplayInterface, StdOutDisplayInterface
{
	public void WriteToFile(String s)
	{
		try
		{
			File f = new File("output.txt");
			FileOutputStream fos = new FileOutputStream(f);
			PrintWriter pw = new PrintWriter(fos);

			pw.write(s);
			pw.flush();
			fos.close();
			pw.close();
		} catch (Exception e)
		{
			System.out.println("Unable to write to file");
		}
	}
	
	public void writeToStdout(String s)
	{
		
	}

}
