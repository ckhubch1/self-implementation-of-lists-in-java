import java.io.*;
import java.util.*;

public class FileProcessor
{
	private Scanner x,x2;
	public static String fileName = "input.txt";
	public static int count = 0;

	public void OpenFile()
	{

		try
		{
			x = new Scanner(new File(fileName));
			x2 = new Scanner(new File(fileName));
		} 
		catch (Exception e)
		{
			System.out.println("Unable to open file '" + fileName + "'");
		}
	}

	public int CountLines()
	{
		while(x.hasNext())
		{
			count++;
			x.nextLine();
		}
		return count;
	}
	
	public String readLine()
	{
		String line = null;
		line = x2.next();
		return line;
	}
	
	public void CloseFile()
	{
		x.close();
	}
}
		
