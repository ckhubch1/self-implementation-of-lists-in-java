package myArrayList.driver;
import myArrayList.MyArrayList;

public class Driver
{
	public static int count;
	public static int value;
	public static int arraySize;
	
	public static void main(String args[])
	{
		FileProcessor FPObj = new FileProcessor();
		MyArrayList MAObj = new MyArrayList();
		Results RObj = new Results();
		MAObj.assignArray();
		FPObj.OpenFile();
		count = FPObj.CountLines();
		arraySize = MAObj.arraySize();
		for (int i = 0; i < count; i++)
		{
			value = Integer.parseInt(FPObj.readLine());
			MAObj.insertSorted(value);
		}
		FPObj.CloseFile();
		MAObj.insertSorted(55);
		MAObj.removeValue(55);
		MAObj.insertSorted(55);
		int sum = MAObj.sum();
		String output = "The sum of all the values in the array list is: " +sum;
		RObj.WriteToFile(output);
		MAObj.toString();
	}

}
