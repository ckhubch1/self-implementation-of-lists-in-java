/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author chira
 */
package myArrayList.test;
import java.util.Arrays;

import myArrayList.MyArrayList;
import myArrayList.util.Results;

public class MyArrayListTest 
{
    public void testMe(MyArrayList myArrayList, Results results)
    {
        test1(myArrayList, results);
        test2(myArrayList, results);
       test3(myArrayList, results);
        test4(myArrayList, results);
        test5(myArrayList, results);
        test6(myArrayList, results);
       test7(myArrayList, results);
        test8(myArrayList, results);
        test9(myArrayList, results);
        test10(myArrayList, results);
    }
    
    public void test1(MyArrayList myArrayList, Results results)
    {
        String expected = "10";
        String test = "\nTest Case:1 Inserting element into array. \nResult: ";
        myArrayList.insertSorted(10);
        results.storeNewResult(test + "Passed");
        System.out.print("Output Result: ");
        myArrayList.toString();
        System.out.print("\n");
       
    }
    
      public void test2(MyArrayList myArrayList, Results results)
    {
        String expected = "1 5 10 12 15 100";
        String test = "\nTest Case:2 Inserting 5 more elements into array. \nResult: ";
        myArrayList.insertSorted(5);
        myArrayList.insertSorted(1);
        myArrayList.insertSorted(15);
        myArrayList.insertSorted(12);
        myArrayList.insertSorted(100);
        results.storeNewResult(test + "Passed");
        System.out.print("Output Result: ");
        myArrayList.toString();
        System.out.print("\n");
       
    }
    
    
    public void test3(MyArrayList myArrayList, Results results)
    {
        String expected = "1 5 10 12 15";
        String test = "\nTest Case:3 Removing element from array. \nResult:";
        myArrayList.removeValue(100);
        results.storeNewResult(test + "Passed");
        System.out.print("Output Result: ");
        myArrayList.toString();
        System.out.print("\n");
       
    }
    
     public void test4(MyArrayList myArrayList, Results results)
    {
        String expected = "1 5 10 12";
        String test = "\nTest Case:4 Retrieving Size of Array after removing element. \nResult:";
        myArrayList.removeValue(15);
        int size = myArrayList.size();
        results.storeNewResult(test + "Passed");
        System.out.print("Output Result: ");
        myArrayList.toString();
        System.out.print("\nSize of Array: "+size);
        System.out.print("\n");
       
    }
      public void test5(MyArrayList myArrayList, Results results)
    {
        String expected = "1 4 5 10 12";
        String test = "\nTest Case:5 Finding Index of element after adding that element. \nResult:";
        myArrayList.insertSorted(4);
        int index = myArrayList.indexOf(4);
        results.storeNewResult(test + "Passed");
        System.out.print("Output Result: ");
        myArrayList.toString();
        System.out.print("\nIndex of integer 4: "+index);
        System.out.print("\n");
       
    }
       public void test6(MyArrayList myArrayList, Results results)
    {
        String expected = "1 5 10 12";
        String test = "\nTest Case:6 Removing element from array. Then finding it's index. \nResult:";
        myArrayList.removeValue(15);
       int index = myArrayList.indexOf(15);
        results.storeNewResult(test + "Passed");
        System.out.print("Output Result: ");
        myArrayList.toString();
         System.out.print("\nIndex of integer 15: "+index);
        System.out.print("\n");
       
    }
        public void test7(MyArrayList myArrayList, Results results)
    {
        String expected = "1 5 10 12 20";
        String test = "\nTest Case:7 Basic test case for insert and sum. \nResult:";
        myArrayList.insertSorted(20);
        int sum = myArrayList.sum();
        results.storeNewResult(test + "Passed");
        System.out.print("Output Result: ");
        myArrayList.toString();
        System.out.print("\nSum: "+sum);
        System.out.print("\n");
       
    }
         public void test8(MyArrayList myArrayList, Results results)
    {
        String expected = "1 4 5 10 12 20 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25";
        String test = "\nTest Case:8 Inserting more than array size. \nResult:";
        for(int i=0; i<60; i++)
        {
            myArrayList.insertSorted(25);
        }
         int size= myArrayList.size();
        results.storeNewResult(test + "Passed");
        System.out.print("Output Result: ");
        myArrayList.toString();
        System.out.print("\nSize: "+size);
        System.out.print("\n");
       
    }
          public void test9(MyArrayList myArrayList, Results results)
    {
        String expected = "1 4 5 10 12 20 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25";
        String test = "\nTest Case:9  Return index of a number with duplicates in list. \nResult:";
        int index = myArrayList.indexOf(25);
        results.storeNewResult(test + "Passed");
        System.out.print("Output Result: ");
        myArrayList.toString();
        System.out.print("\nIndex: "+index);
        System.out.print("\n");
       
    }
           public void test10(MyArrayList myArrayList, Results results)
    {
        String expected = "1 5 10 12 15";
        String test = "\nTest Case:10 Adding, Removing and then Calculating Sum. \nResult:";
        myArrayList.insertSorted(2);
        myArrayList.removeValue(10);
        int sum = myArrayList.sum();
        results.storeNewResult(test + "Passed");
        System.out.print("Output Result: ");
        myArrayList.toString();
        System.out.print("\nSum: "+sum);
       
        System.out.print("\n");
       
    }

    }

